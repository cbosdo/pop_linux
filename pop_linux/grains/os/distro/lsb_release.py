import logging
import re
import os

log = logging.getLogger(__name__)

# Matches any possible format:
#     DISTRIB_ID="Ubuntu"
#     DISTRIB_ID='Mageia'
#     DISTRIB_ID=Fedora
#     DISTRIB_RELEASE='10.10'
#     DISTRIB_CODENAME='squeeze'
#     DISTRIB_DESCRIPTION='Ubuntu 10.10'
_LSB_REGEX = re.compile((
    '^(DISTRIB_(?:ID|RELEASE|CODENAME|DESCRIPTION))=(?:\'|")?'
    '([\\w\\s\\.\\-_]+)(?:\'|")?'
))


async def _parse_lsb_release():
    ret = {}
    try:
        log.debug('Attempting to parse /etc/lsb-release')
        with open('/etc/lsb-release') as ifile:
            for line in ifile:
                try:
                    key, value = _LSB_REGEX.match(line.rstrip('\n')).groups()[:2]
                except AttributeError:
                    pass
                else:
                    # Adds lsb_distrib_{id,release,codename,description}
                    ret[f'lsb_{key.lower()}'] = value.rstrip()
    except (IOError, OSError) as exc:
        log.debug('Failed to parse /etc/lsb-release: %s', exc)
    return ret


async def _parse_os_release(*os_release_files):
    '''
    Parse os-release and return a parameter dictionary

    See http://www.freedesktop.org/software/systemd/man/os-release.html
    for specification of the file format.
    '''
    ret = {}
    for filename in os_release_files:
        try:
            with open(filename) as ifile:
                regex = re.compile('^([\\w]+)=(?:\'|")?(.*?)(?:\'|")?$')
                for line in ifile:
                    match = regex.match(line.strip())
                    if match:
                        # Shell special characters ("$", quotes, backslash,
                        # backtick) are escaped with backslashes
                        ret[match.group(1)] = re.sub(
                            r'\\([$"\'\\`])', r'\1', match.group(2)
                        )
            break
        except (IOError, OSError):
            pass

    return ret


async def _parse_cpe_name(cpe):
    '''
    Parse CPE_NAME data from the os-release

    Info: https://csrc.nist.gov/projects/security-content-automation-protocol/scap-specifications/cpe

    Note: cpe:2.3:part:vendor:product:version:update:edition:lang:sw_edition:target_sw:target_hw:other
          however some OS's do not have the full 13 elements, for example:
                CPE_NAME="cpe:2.3:o:amazon:amazon_linux:2"

    :param cpe:
    :return:
    '''
    part = {
        'o': 'operating system',
        'h': 'hardware',
        'a': 'application',
    }
    ret = {}
    cpe = (cpe or '').split(':')
    if len(cpe) > 4 and cpe[0] == 'cpe':
        if cpe[1].startswith('/'):  # WFN to URI
            ret['vendor'], ret['product'], ret['version'] = cpe[2:5]
            ret['phase'] = cpe[5] if len(cpe) > 5 else None
            ret['part'] = part.get(cpe[1][1:])
        elif len(cpe) == 6 and cpe[1] == '2.3':  # WFN to a string
            ret['vendor'], ret['product'], ret['version'] = [x if x != '*' else None for x in cpe[3:6]]
            ret['phase'] = None
            ret['part'] = part.get(cpe[2])
        elif len(cpe) > 7 and len(cpe) <= 13 and cpe[1] == '2.3':  # WFN to a string
            ret['vendor'], ret['product'], ret['version'], ret['phase'] = [x if x != '*' else None for x in cpe[3:7]]
            ret['part'] = part.get(cpe[2])

    return ret


async def load_lsb_release(hub):
    # Add lsb grains on any distro with lsb-release. Note that this import
    # can fail on systems with lsb-release installed if the system package
    # does not install the python package for the python interpreter used by
    # Salt (i.e. python2 or python3)
    try:
        log.debug('Getting lsb_release distro information')
        import lsb_release  # pylint: disable=import-error
        release = lsb_release.get_distro_information()
        for key, value in release.items():
            key = key.lower()
            distrib = '' if key.startswith('distrib_') else 'distrib_'
            lsb_param = f'lsb_{distrib}{key}'
            hub.grains.GRAINS[lsb_param] = value
    # Catch a NameError to workaround possible breakage in lsb_release
    # See https://github.com/saltstack/salt/issues/37867
    except (ImportError, NameError):
        # if the python library isn't available, try to parse
        # /etc/lsb-release using regex
        log.debug('lsb_release python bindings not available')
        hub.grains.GRAINS.update(await _parse_lsb_release())

        if hub.grains.GRAINS.get('lsb_distrib_description', '').lower().startswith('antergos'):
            # Antergos incorrectly configures their /etc/lsb-release,
            # setting the DISTRIB_ID to "Arch". This causes the "os" grain
            # to be incorrectly set to "Arch".
            hub.grains.GRAINS['osfullname'] = 'Antergos Linux'
        elif 'lsb_distrib_id' not in hub.grains.GRAINS:
            log.debug(
                'Failed to get lsb_distrib_id, trying to parse os-release'
            )
            os_release = await _parse_os_release('/etc/os-release', '/usr/lib/os-release')
            if os_release:
                if 'NAME' in os_release:
                    hub.grains.GRAINS['lsb_distrib_id'] = os_release['NAME'].strip()
                if 'VERSION_ID' in os_release:
                    hub.grains.GRAINS['lsb_distrib_release'] = os_release['VERSION_ID']
                if 'VERSION_CODENAME' in os_release:
                    hub.grains.GRAINS['lsb_distrib_codename'] = os_release['VERSION_CODENAME']
                elif 'PRETTY_NAME' in os_release:
                    codename = os_release['PRETTY_NAME']
                    # https://github.com/saltstack/salt/issues/44108
                    if os_release['ID'] == 'debian':
                        codename_match = re.search(r'\((\w+)\)$', codename)
                        if codename_match:
                            codename = codename_match.group(1)
                    hub.grains.GRAINS['lsb_distrib_codename'] = codename
                if 'CPE_NAME' in os_release:
                    cpe = await _parse_cpe_name(os_release['CPE_NAME'])
                    if not cpe:
                        log.error('Broken CPE_NAME format in /etc/os-release!')
                    elif cpe.get('vendor', '').lower() in ['suse', 'opensuse']:
                        hub.grains.GRAINS['os'] = "SUSE"
                        # openSUSE `osfullname` grain normalization
                        if os_release.get("NAME") == "openSUSE Leap":
                            hub.grains.GRAINS['osfullname'] = "Leap"
                        elif os_release.get("VERSION") == "Tumbleweed":
                            hub.grains.GRAINS['osfullname'] = os_release["VERSION"]
                        # Override VERSION_ID, if CPE_NAME around
                        if cpe.get('version') and cpe.get('vendor') == 'opensuse':  # Keep VERSION_ID for SLES
                            hub.grains.GRAINS['lsb_distrib_release'] = cpe['version']

            elif os.path.isfile('/etc/SuSE-release'):
                log.debug('Parsing distrib info from /etc/SuSE-release')
                hub.grains.GRAINS['lsb_distrib_id'] = 'SUSE'
                version = ''
                patch = ''
                with open('/etc/SuSE-release') as fhr:
                    for line in fhr:
                        if 'enterprise' in line.lower():
                            hub.grains.GRAINS['lsb_distrib_id'] = 'SLES'
                            hub.grains.GRAINS['lsb_distrib_codename'] = re.sub(r'\(.+\)', '', line).strip()
                        elif 'version' in line.lower():
                            version = re.sub(r'[^0-9]', '', line)
                        elif 'patchlevel' in line.lower():
                            patch = re.sub(r'[^0-9]', '', line)
                hub.grains.GRAINS['lsb_distrib_release'] = version
                if patch:
                    hub.grains.GRAINS['lsb_distrib_release'] += '.' + patch
                    patchstr = 'SP' + patch
                    if hub.grains.GRAINS['lsb_distrib_codename'] and patchstr not in hub.grains.GRAINS[
                        'lsb_distrib_codename']:
                        hub.grains.GRAINS['lsb_distrib_codename'] += ' ' + patchstr
                if not hub.grains.GRAINS.get('lsb_distrib_codename'):
                    hub.grains.GRAINS['lsb_distrib_codename'] = 'n.a'
            elif os.path.isfile('/etc/altlinux-release'):
                log.debug('Parsing distrib info from /etc/altlinux-release')
                # ALT Linux
                hub.grains.GRAINS['lsb_distrib_id'] = 'altlinux'
                with open('/etc/altlinux-release') as ifile:
                    # This file is symlinked to from:
                    #     /etc/fedora-release
                    #     /etc/redhat-release
                    #     /etc/system-release
                    for line in ifile:
                        # ALT Linux Sisyphus (unstable)
                        comps = line.split()
                        if comps[0] == 'ALT':
                            hub.grains.GRAINS['lsb_distrib_release'] = comps[2]
                            hub.grains.GRAINS['lsb_distrib_codename'] = \
                                comps[3].replace('(', '').replace(')', '')
            elif os.path.isfile('/etc/centos-release'):
                log.debug('Parsing distrib info from /etc/centos-release')
                # CentOS Linux
                hub.grains.GRAINS['lsb_distrib_id'] = 'CentOS'
                with open('/etc/centos-release') as ifile:
                    for line in ifile:
                        # Need to pull out the version and codename
                        # in the case of custom content in /etc/centos-release
                        find_release = re.compile(r'\d+\.\d+')
                        find_codename = re.compile(r'(?<=\()(.*?)(?=\))')
                        release = find_release.search(line)
                        codename = find_codename.search(line)
                        if release is not None:
                            hub.grains.GRAINS['lsb_distrib_release'] = release.group()
                        if codename is not None:
                            hub.grains.GRAINS['lsb_distrib_codename'] = codename.group()
            elif os.path.isfile('/etc.defaults/VERSION') \
                    and os.path.isfile('/etc.defaults/synoinfo.conf'):
                hub.grains.GRAINS['osfullname'] = 'Synology'
                log.debug('Parsing Synology distrib info from /etc/.defaults/VERSION')
                with open('/etc.defaults/VERSION', 'r') as fp_:
                    synoinfo = {}
                    for line in fp_:
                        try:
                            key, val = line.rstrip('\n').split('=')
                        except ValueError:
                            continue
                        if key in ('majorversion', 'minorversion', 'buildnumber'):
                            synoinfo[key] = val.strip('"')
                    if len(synoinfo) != 3:
                        log.warning(
                            'Unable to determine Synology version info. '
                            'Please report this, as it is likely a bug.'
                        )
                    else:
                        hub.grains.GRAINS['osrelease'] = (
                            f'{synoinfo["majorversion"]}.{synoinfo["minorversion"]}-{synoinfo["buildnumber"]}'
                        )
