import logging
import re
import warnings

# linux_distribution deprecated in py3.7
try:
    from platform import linux_distribution as _deprecated_linux_distribution


    def _linux_distribution(**kwargs):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            return _deprecated_linux_distribution(**kwargs)
except ImportError:
    from distro import linux_distribution as _linux_distribution

# Extend the default list of supported distros. This will be used for the
# /etc/DISTRO-release checking that is part of linux_distribution()
from platform import _supported_dists
_supported_dists += ('arch', 'mageia', 'meego', 'vmware', 'bluewhite64',
                     'slamd64', 'ovs', 'system', 'mint', 'oracle', 'void')

log = logging.getLogger(__name__)

_REPLACE_LINUX_RE = re.compile(r'\W(?:gnu/)?linux', re.IGNORECASE)

_OS_NAME_MAP = {
    'redhatente': 'RedHat',
    'gentoobase': 'Gentoo',
    'archarm': 'Arch ARM',
    'arch': 'Arch',
    'debian': 'Debian',
    'raspbian': 'Raspbian',
    'fedoraremi': 'Fedora',
    'chapeau': 'Chapeau',
    'korora': 'Korora',
    'amazonami': 'Amazon',
    'alt': 'ALT',
    'enterprise': 'OEL',
    'oracleserv': 'OEL',
    'cloudserve': 'CloudLinux',
    'cloudlinux': 'CloudLinux',
    'pidora': 'Fedora',
    'scientific': 'ScientificLinux',
    'synology': 'Synology',
    'nilrt': 'NILinuxRT',
    'poky': 'Poky',
    'manjaro': 'Manjaro',
    'manjarolin': 'Manjaro',
    'univention': 'Univention',
    'antergos': 'Antergos',
    'sles': 'SUSE',
    'void': 'Void',
    'slesexpand': 'RES',
    'linuxmint': 'Mint',
    'neon': 'KDE neon',
}



async def load_distro(hub):
    # Use the already intelligent platform module to get distro info
    # (though apparently it's not intelligent enough to strip quotes)
    log.debug(
        'Getting OS name, release, and codename from '
        'platform.linux_distribution()'
    )
    (osname, osrelease, oscodename) = \
        [x.strip('"').strip("'") for x in
         _linux_distribution(supported_dists=_supported_dists)]

    hub.grains.GRAINS['osfullname'] = osname.strip()
    hub.grains.GRAINS['osrelease'] = osrelease.strip()
    hub.grains.GRAINS['oscodename'] = oscodename.strip()

    if 'Red Hat' in hub.grains.GRAINS['oscodename']:
        hub.grains.GRAINS['oscodename'] = oscodename

    distroname = _REPLACE_LINUX_RE.sub('', hub.grains.GRAINS['osfullname']).strip()
    # return the first ten characters with no spaces, lowercased
    shortname = distroname.replace(' ', '').lower()[:10]
    # this maps the long names from the /etc/DISTRO-release files to the
    # traditional short names that Salt has used.
    hub.grains.GRAINS['os'] = _OS_NAME_MAP.get(shortname, distroname)
