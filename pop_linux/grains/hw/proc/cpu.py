import os


async def load_cpuinfo(hub):
    hub.grains.GRAINS['num_cpus'] = 0
    hub.grains.GRAINS['cpu_model'] = 'Unknown'
    hub.grains.GRAINS['cpu_flags'] = []

    cpuinfo = '/proc/cpuinfo'
    if os.path.isfile(cpuinfo):
        # Parse over the cpuinfo file
        with open(cpuinfo, 'r') as _fp:
            for line in _fp:
                comps = line.split(':')
                if not len(comps) > 1:
                    continue
                key = comps[0].strip()
                val = comps[1].strip()
                if key == 'processor':
                    hub.grains.GRAINS['num_cpus'] = int(val) + 1
                # head -2 /proc/cpuinfo
                # vendor_id       : IBM/S390
                # # processors    : 2
                elif key == '# processors':
                    hub.grains.GRAINS['num_cpus'] = int(val)
                elif key == 'vendor_id':
                    hub.grains.GRAINS['cpu_model'] = val
                elif key == 'model name':
                    hub.grains.GRAINS['cpu_model'] = val
                elif key == 'flags':
                    hub.grains.GRAINS['cpu_flags'] = sorted(val.split())
                elif key == 'Features':
                    hub.grains.GRAINS['cpu_flags'] = sorted(val.split())
                # ARM support - /proc/cpuinfo
                #
                # Processor       : ARMv6-compatible processor rev 7 (v6l)
                # BogoMIPS        : 697.95
                # Features        : swp half thumb fastmult vfp edsp java tls
                # CPU implementer : 0x41
                # CPU architecture: 7
                # CPU variant     : 0x0
                # CPU part        : 0xb76
                # CPU revision    : 7
                #
                # Hardware        : BCM2708
                # Revision        : 0002
                # Serial          : 00000000
                elif key == 'Processor':
                    hub.grains.GRAINS['cpu_model'] = val.split('-')[0]
                    hub.grains.GRAINS['num_cpus'] = 1

        # Report if hardware virtualization is available under amd or intel
        hub.grains.GRAINS['hardware_virtualization'] = any(
            flag in hub.grains.GRAINS['cpu_flags'] for flag in ('svn', 'vmx')
        )

