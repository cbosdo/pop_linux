async def load_ps(hub):
    if hub.grains.GRAINS.get('virtual', '') == 'openvzhn':
        hub.grains.GRAINS['ps'] = (
            'ps -fH -p $(grep -l \"^envID:[[:space:]]*0\\$\" '
            '/proc/[0-9]*/status | sed -e \"s=/proc/\\([0-9]*\\)/.*=\\1=\")  '
            '| awk \'{ $7=\"\"; print }\''
        )
    elif hub.grains.GRAINS['os_family'] == 'NILinuxRT':
        hub.grains.GRAINS['ps'] = 'ps -o user,pid,ppid,tty,time,comm'
    else:
        hub.grains.GRAINS['ps'] = 'ps -efHww'
