import re
import os


async def load_vz(hub):
    if hub.grains.GRAINS.get('virtual'):
        return

    if os.path.isfile('/proc/vz/version'):
        hub.grains.GRAINS['virtual'] = 'openvzhn'
    elif os.path.isfile('/proc/vz/veinfo'):
        hub.grains.GRAINS['virtual'] = 'openvzve'
        # a posteriori, it's expected for these to have failed:


async def load_self_status(hub):
    if hub.grains.GRAINS.get('virtual'):
        return

    name = '/proc/self/status'
    if os.path.isfile(name):
        with open(name) as status_file:
            vz_re = re.compile(r'^envID:\s+(\d+)$')
            for line in status_file:
                vz_match = vz_re.match(line.rstrip('\n'))
                if vz_match and int(vz_match.groups()[0]) != 0:
                    hub.grains.GRAINS['virtual'] = 'openvzve'
                elif vz_match and int(vz_match.groups()[0]) == 0:
                    hub.grains.GRAINS['virtual'] = 'openvzhn'


async def load_xen(hub):
    if os.path.isdir('/proc/sys/xen') or \
            os.path.isdir('/sys/bus/xen') or os.path.isdir('/proc/xen'):
        if os.path.isfile('/proc/xen/xsd_kva'):
            # Tested on CentOS 5.3 / 2.6.18-194.26.1.el5xen
            # Tested on CentOS 5.4 / 2.6.18-164.15.1.el5xen
            hub.grains.GRAINS['virtual_subtype'] = 'Xen Dom0'
        else:
            if hub.grains.GRAINS.get('productname', '') == 'HVM domU':
                # Requires dmidecode!
                hub.grains.GRAINS['virtual_subtype'] = 'Xen HVM DomU'
            elif os.path.isfile('/proc/xen/capabilities') and \
                    os.access('/proc/xen/capabilities', os.R_OK):
                with open('/proc/xen/capabilities') as fhr:
                    if 'control_d' not in fhr.read():
                        # Tested on CentOS 5.5 / 2.6.18-194.3.1.el5xen
                        hub.grains.GRAINS['virtual_subtype'] = 'Xen PV DomU'
                    else:
                        # Shouldn't get to this, but just in case
                        hub.grains.GRAINS['virtual_subtype'] = 'Xen Dom0'
            # Tested on Fedora 10 / 2.6.27.30-170.2.82 with xen
            # Tested on Fedora 15 / 2.6.41.4-1 without running xen
            elif os.path.isdir('/sys/bus/xen'):
                if 'xen:' in (await hub.exec.cmd.run('dmesg'))['stdout'].lower():
                    hub.grains.GRAINS['virtual_subtype'] = 'Xen PV DomU'
                elif os.path.isfile('/sys/bus/xen/drivers/xenconsole'):
                    # An actual DomU will have the xenconsole driver
                    hub.grains.GRAINS['virtual_subtype'] = 'Xen PV DomU'
        # If a Dom0 or DomU was detected, obviously this is xen
        if 'dom' in hub.grains.GRAINS.get('virtual_subtype', '').lower():
            hub.grains.GRAINS['virtual'] = 'xen'


async def load_1_root(hub):
    if hub.grains.GRAINS.get('virtual_subtype'):
        return

    if os.path.isdir('/proc'):
        try:
            self_root = os.stat('/')
            init_root = os.stat('/proc/1/root/.')
            if self_root != init_root:
                hub.grains.GRAINS['virtual_subtype'] = 'chroot'
        except (IOError, OSError):
            pass


async def load_cgroup(hub):
    name = '/proc/1/cgroup'
    if os.path.isfile(name):
        with open(name, 'r') as fhr:
            fhr_contents = fhr.read()
        if ':/lxc/' in fhr_contents:
            hub.grains.GRAINS['virtual'] = 'container'
            hub.grains.GRAINS['virtual_subtype'] = 'LXC'
        elif ':/kubepods/' in fhr_contents:
            hub.grains.GRAINS['virtual_subtype'] = 'kubernetes'
        elif ':/libpod_parent/' in fhr_contents:
            hub.grains.GRAINS['virtual_subtype'] = 'libpod'
        else:
            if any(x in fhr_contents
                   for x in (':/system.slice/docker', ':/docker/',
                             ':/docker-ce/')):
                hub.grains.GRAINS['virtual'] = 'container'
                hub.grains.GRAINS['virtual_subtype'] = 'Docker'


async def load_cpuinfo(hub):
    name = '/proc/cpuinfo'
    if os.path.isfile(name):
        with open(name, 'r') as fhr:
            if 'QEMU Virtual CPU' in fhr.read():
                hub.grains.GRAINS['virtual'] = 'kvm'


async def load_product_name(hub):
    name = '/sys/devices/virtual/dmi/id/product_name'
    if os.path.isfile(name):
        try:
            with open(name, 'r') as fhr:
                output = fhr.read()
                if 'VirtualBox' in output:
                    hub.grains.GRAINS['virtual'] = 'VirtualBox'
                elif 'RHEV Hypervisor' in output:
                    hub.grains.GRAINS['virtual'] = 'kvm'
                    hub.grains.GRAINS['virtual_subtype'] = 'rhev'
                elif 'oVirt Node' in output:
                    hub.grains.GRAINS['virtual'] = 'kvm'
                    hub.grains.GRAINS['virtual_subtype'] = 'ovirt'
                elif 'Google' in output:
                    hub.grains.GRAINS['virtual'] = 'gce'
                elif 'BHYVE' in output:
                    hub.grains.GRAINS['virtual'] = 'bhyve'
        except IOError:
            pass
