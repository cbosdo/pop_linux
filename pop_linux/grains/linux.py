import sys


async def is_linux(hub):
    '''
    Verify that POP linux is running on linux
    '''
    if not sys.platform.startswith('linux'):
        raise OSError('POP-linux only runs on the linux kernel')
    return True
