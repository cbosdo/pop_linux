import sys


async def load_python_version(hub):
    hub.grains.GRAINS['pythonversion'] = list(sys.version_info)


async def load_zmq_version(hub):
    '''
    Return the zeromq version
    '''
    # Provides:
    #   zmqversion
    try:
        import zmq
        hub.grains.GRAINS['zmqversion'] = zmq.zmq_version()  # pylint: disable=no-member
    except ImportError:
        pass
