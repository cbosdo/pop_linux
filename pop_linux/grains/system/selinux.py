import logging
import shutil

log = logging.getLogger(__name__)


async def load_selinux(hub):
    if shutil.which('selinuxenabled'):
        log.debug('Adding selinux grains')
        hub.grains.GRAINS['selinux'] = {}
        hub.grains.GRAINS['selinux']['enabled'] = hub.exec.cmd.retcode('selinuxenabled') == 0

        if shutil.which('getenforce'):
            hub.grains.GRAINS['selinux']['enforced'] = hub.exec.cmd.run('getenforce').strip()
