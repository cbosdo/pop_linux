import socket


async def _get_fqhostname():
    '''
    Returns the fully qualified hostname
    '''
    l = [socket.getfqdn()]

    # try socket.getaddrinfo
    try:
        addrinfo = socket.getaddrinfo(
            socket.gethostname(), 0, socket.AF_UNSPEC, socket.SOCK_STREAM,
            socket.SOL_TCP, socket.AI_CANONNAME
        )
        for info in addrinfo:
            # info struct [family, socktype, proto, canonname, sockaddr]
            if len(info) >= 4:
                l.append(info[3])
    except socket.gaierror:
        pass

    return l and l[0] or None


async def load_names(hub):
    '''
    Return fqdn, hostname, domainname
    '''
    # This is going to need some work
    # Provides:
    #   fqdn
    #   host
    #   localhost
    #   domain
    hub.grains.GRAINS['localhost'] = socket.gethostname()

    # On some distros (notably FreeBSD) if there is no hostname set _get_fqhostname() will return None.
    # In this case we punt and log a message at error level, but force the
    # hostname and domain to be localhost.localdomain
    # Otherwise we would stacktrace below
    fqdn = (await _get_fqhostname()) or 'localhost.localdomain'

    hub.grains.GRAINS['fqdn'] = fqdn
    hub.grains.GRAINS['host'], hub.grains.GRAINS['domain'] = hub.grains.GRAINS['fqdn'].partition('.')[::2]
