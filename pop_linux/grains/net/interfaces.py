import ifcfg
import itertools


async def load_interfaces(hub):
    '''
    Provide a dict of the connected interfaces and their ip addresses
    The addresses will be passed as a list for each interface
    '''
    # Provides:
    #   ip_interfaces
    hub.grains.GRAINS['hw_addr_interfaces'] = {}
    hub.grains.GRAINS['ip_interfaces'] = {}
    hub.grains.GRAINS['ip4_interfaces'] = {}
    hub.grains.GRAINS['ip6_interfaces'] = {}
    hub.grains.GRAINS['ipv4'] = []
    hub.grains.GRAINS['ipv6'] = []

    ifaces = ifcfg.interfaces()
    for face in ifaces:
        hub.grains.GRAINS['hw_addr_interfaces'][face] = ifaces[face].get('ether', '00:00:00:00:00:00')
        inet4 = ifaces[face].get('inet4', [])
        hub.grains.GRAINS['ipv4'].extend(inet4)
        hub.grains.GRAINS['ip4_interfaces'][face] = inet4
        inet6 = ifaces[face].get('inet6', [])
        hub.grains.GRAINS['ipv6'].extend(inet6)
        hub.grains.GRAINS['ip6_interfaces'][face] = inet6
        hub.grains.GRAINS['ip_interfaces'][face] = hub.grains.GRAINS['ip4_interfaces'][face] + hub.grains.GRAINS['ip6_interfaces'][face]
