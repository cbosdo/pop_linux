import datetime
import logging
import socket

log = logging.getLogger(__name__)


async def load_fqdn_ip(hub):
    _fqdn = hub.grains.GRAINS['fqdn']
    for socket_type, ipv_num in ((socket.AF_INET, '4'), (socket.AF_INET6, '6')):
        key = 'fqdn_ip' + ipv_num
        if not hub.grains.GRAINS['ipv' + ipv_num]:
            hub.grains.GRAINS[key] = []
        else:
            try:
                start_time = datetime.datetime.utcnow()
                info = socket.getaddrinfo(_fqdn, None, socket_type)
                hub.grains.GRAINS[key] = list(set(item[4][0] for item in info))
            except socket.error:
                timediff = datetime.datetime.utcnow() - start_time
                if timediff.seconds > 5:
                    log.warning(
                        'Unable to find IPv%s record for "%s" causing a %s '
                        'second timeout when rendering grains. Set the dns or '
                        '/etc/hosts for IPv%s to clear this.',
                        ipv_num, _fqdn, timediff, ipv_num
                    )
                hub.grains.GRAINS[key] = []
